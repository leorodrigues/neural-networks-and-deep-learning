# Neural Networks and Deep Learning

This project is my interpretation of the work of [Michael Nielsen][2]
in his book ["Neural Networks and Deep Learning"][1].

I advise to NOT USE THIS CODE IN PRODUCTION. It is experimental code I used
to learn C++ and neural netwtorks as I read [Nielsen's book][1].

The source code included in this repository under the directory ["mnielsen"][3] was
writen by Nielsen and actually belongs to him. Its repository is [here][3].

## Technical details

*Operating system*

    Debian 10.5 / 4.19.132-1 (2020-07-24) x86_64 GNU/Linux
    Linux 4.19.0-10-amd64 SMP

*Libraries*

    Google Test 1.7.0 / Google Mock 1.7.0 - Unit testing
    Eigen 3.3.7 - Linear algebra library

*Build/compile tools*

    CMake 3.13.4 / g++ 8.3.0 / GNU ld 2.31.1

*CPU*

    Intel(R) Core(TM) i7-8565U CPU @ 1.80GHz

[1]: http://neuralnetworksanddeeplearning.com/index.html "The actual book"
[2]: http://michaelnielsen.org/ "Nielsen's personal web page"
[3]: https://github.com/mnielsen/neural-networks-and-deep-learning "Actual sources from Nielsen"
