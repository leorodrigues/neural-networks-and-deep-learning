#ifndef __INTERFACES__
#define __INTERFACES__

#include <eigen3/Eigen/Dense>
#include <vector>
#include <memory>
#include <tuple>

using namespace Eigen;
using namespace std;

class CostFunction {
    public:
        virtual double invoke() = 0;
};

class LearningFunction {
    public:
        virtual double invoke() = 0;
};

class OutputFunction {
    public:
        virtual MatrixXd apply(const MatrixXd& lineVector) = 0;
        virtual MatrixXd applyDerived(const MatrixXd& lineVector) = 0;
};

class Layer {
    public:
        virtual MatrixXd computeNetInput(const MatrixXd& input) = 0;
        virtual MatrixXd computeOutput(const MatrixXd& netInput) = 0;
        virtual MatrixXd computeDerivedOutput(const MatrixXd& netInput) = 0;

        virtual MatrixXd adjustWeightsToBaseError(
            const MatrixXd& expectation,
            const MatrixXd& output,
            const MatrixXd& derivedOutput) = 0;

        virtual MatrixXd adjustWeightsToIntermediaryError(
            const MatrixXd &nextLayerError,
            const MatrixXd &output,
            const MatrixXd &derivedOutput) = 0;
};

class Network {
    public:
        virtual MatrixXd reactToInput(const MatrixXd& input) = 0;
        virtual MatrixXd learnFromSample(const MatrixXd& sample) = 0;
};

class LayerFactory {
    public:
        virtual shared_ptr<Layer> makeLayer(
            uint32_t inputSize, uint32_t numberOfNeurons) = 0;
};

class NetworkFactory {
    public:
        virtual shared_ptr<Network> makeNetwork(
            const vector<uint32_t>& layerSizes) = 0;
};

class LayerChainLink {
    public:
        virtual MatrixXd feedForward(
            const MatrixXd& input) = 0;

        virtual void backPropagate(
            const MatrixXd& input,
            const MatrixXd& expectations) = 0;
};

#endif