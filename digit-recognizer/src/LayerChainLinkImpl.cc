#include "LayerChainLinkImpl.hh"

MatrixXd LayerChainLinkImpl::feedForward(const MatrixXd& input) {
    return nextLink == nullptr ? activateSelf(input) : activateNext(input);
}

void LayerChainLinkImpl::backPropagate(
    const MatrixXd& input, const MatrixXd& expectation) {

    propagateDownChain(input, expectation);
}

MatrixXd LayerChainLinkImpl::activateSelf(const MatrixXd& input) {
    return (*delegate).computeOutput((*delegate).computeNetInput(input));
}

MatrixXd LayerChainLinkImpl::activateNext(const MatrixXd& input) {
    return (*nextLink).feedForward(activateSelf(input));
}

MatrixXd LayerChainLinkImpl::propagateDownChain(
    const MatrixXd& input, const MatrixXd& expectation) {

    auto netInput = delegate->computeNetInput(input);
    auto output = delegate->computeOutput(netInput);
    auto outputPrime = delegate->computeDerivedOutput(netInput);

    if (nextLink == nullptr)
        return delegate->adjustWeightsToBaseError(
            expectation, output, outputPrime);

    return delegate->adjustWeightsToIntermediaryError(
        nextLink->propagateDownChain(output, expectation), output, outputPrime);
}
