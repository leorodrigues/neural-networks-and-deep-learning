#ifndef __LAYER_CHAIN_LINK_IMPL__
#define __LAYER_CHAIN_LINK_IMPL__

#include <utility>
#include <vector>
#include <eigen3/Eigen/Dense>
#include "Interfaces.hh"

using namespace std;
using namespace Eigen;

class LayerChainLinkImpl : public LayerChainLink {
    public:
        explicit LayerChainLinkImpl(
            shared_ptr<Layer> delegate)
                : delegate(move(delegate)) { };

        LayerChainLinkImpl(
            shared_ptr<Layer> delegate,
            shared_ptr<LayerChainLinkImpl> nextLink)
                : delegate(move(delegate)), nextLink(move(nextLink)) { };

        MatrixXd feedForward(
            const MatrixXd& input) override;

        void backPropagate(
            const MatrixXd& input,
            const MatrixXd& expectation) override;

    private:
        shared_ptr<Layer> delegate;
        shared_ptr<LayerChainLinkImpl> nextLink;

        MatrixXd activateSelf(const MatrixXd& input);
        MatrixXd activateNext(const MatrixXd& input);

        MatrixXd propagateDownChain(
            const MatrixXd& input,
            const MatrixXd& expectation);
};

#endif
