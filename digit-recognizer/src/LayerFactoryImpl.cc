
#include "LayerFactoryImpl.hh"
#include "LayerImpl.hh"

LayerFactoryImpl::LayerFactoryImpl(OutputFunction& outputFunction)
    : outputFunction(outputFunction) { }

shared_ptr<Layer> LayerFactoryImpl::makeLayer(
    uint32_t inputSize,
    uint32_t numberOfNeurons) {
    return shared_ptr<Layer>(new LayerImpl(
        inputSize, numberOfNeurons, outputFunction));
}