#ifndef __LAYER_FACTORY_IMPL__
#define __LAYER_FACTORY_IMPL__

#include "Interfaces.hh"

using namespace std;
using namespace Eigen;

class LayerFactoryImpl : public LayerFactory {
    public:
        explicit LayerFactoryImpl(OutputFunction& outputFunction);

        shared_ptr<Layer> makeLayer(
            uint32_t inputSize,
            uint32_t numberOfNeurons) override;

    private:
        OutputFunction& outputFunction;
};

#endif