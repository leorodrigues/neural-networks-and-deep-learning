#include "LayerImpl.hh"

LayerImpl::LayerImpl(
    uint32_t inputSize,
    uint32_t numberOfNeurons,
    OutputFunction& outputFunction)
        : outputFunction(outputFunction) {

    this->biases = MatrixXd::Random(1, numberOfNeurons);
    this->weights = MatrixXd::Random(inputSize, numberOfNeurons);
}

MatrixXd LayerImpl::computeNetInput(const MatrixXd& input) {
    return (input * weights) + biases;
}

MatrixXd LayerImpl::computeOutput(const MatrixXd& netInput) {
    return outputFunction.apply(netInput);
}

MatrixXd LayerImpl::computeDerivedOutput(const MatrixXd& netInput) {
    return outputFunction.applyDerived(netInput);
}

MatrixXd LayerImpl::adjustWeightsToBaseError(
    const MatrixXd& expectation,
    const MatrixXd& output,
    const MatrixXd& derivedOutput) {

    return adjustWeights(output, (output - expectation)
        .cwiseProduct(derivedOutput));
}

MatrixXd LayerImpl::adjustWeightsToIntermediaryError(
    const MatrixXd &nextLayerError,
    const MatrixXd &output,
    const MatrixXd &derivedOutput) {

    return adjustWeights(output, (weights.transpose() * nextLayerError)
        .cwiseProduct(derivedOutput));
}

MatrixXd LayerImpl::adjustWeights(
        const MatrixXd& activations,
        const MatrixXd& errors) {

    this->weights = errors * activations.transpose();
    this->biases = errors;

    return errors;
}
