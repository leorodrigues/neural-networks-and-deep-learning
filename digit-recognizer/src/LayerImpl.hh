#ifndef __LAYER_IMPL__
#define __LAYER_IMPL__

#include <vector>
#include <eigen3/Eigen/Dense>
#include "Interfaces.hh"

using namespace std;
using namespace Eigen;

class LayerImpl : public Layer {
    public:
        LayerImpl(
            uint32_t inputSize,
            uint32_t numberOfNeurons,
            OutputFunction& outputFunction);

        MatrixXd computeNetInput(const MatrixXd& input) override;
        MatrixXd computeOutput(const MatrixXd& netInput) override;
        MatrixXd computeDerivedOutput(const MatrixXd& netInput) override;

        MatrixXd adjustWeightsToBaseError(
            const MatrixXd& expectation,
            const MatrixXd& output,
            const MatrixXd& derivedOutput) override;

        MatrixXd adjustWeightsToIntermediaryError(
            const MatrixXd &nextLayerError,
            const MatrixXd &output,
            const MatrixXd &derivedOutput) override;

    private:
        OutputFunction& outputFunction;
        MatrixXd biases;
        MatrixXd weights;

        MatrixXd adjustWeights(
            const MatrixXd& activations,
            const MatrixXd& errors);
};

#endif
