
#include <memory>

#include "NetworkFactoryImpl.hh"
#include "LayerChainLinkImpl.hh"

using namespace std;

shared_ptr<Network> NetworkFactoryImpl::makeNetwork(
    const vector<uint32_t>& sizes) {

    auto it = sizes.end();
    auto currentLink = makeLink(*(it - 1), *it);

    while (it > sizes.begin()) {
        currentLink = makeLink(*(it - 1), *it, currentLink);
        it--;
    }

    return shared_ptr<Network>(new NetworkImpl(currentLink));
}

shared_ptr<LayerChainLinkImpl> NetworkFactoryImpl::makeLink(
    uint32_t inputSize,
    uint32_t numberOfNeurons) {

    return make_shared<LayerChainLinkImpl>(
        layerFactory.makeLayer(inputSize, numberOfNeurons));
}

shared_ptr<LayerChainLinkImpl> NetworkFactoryImpl::makeLink(
    uint32_t inputSize,
    uint32_t numberOfNeurons,
    shared_ptr<LayerChainLinkImpl> nextLink) {

    return make_shared<LayerChainLinkImpl>(
        layerFactory.makeLayer(inputSize, numberOfNeurons), nextLink);
}