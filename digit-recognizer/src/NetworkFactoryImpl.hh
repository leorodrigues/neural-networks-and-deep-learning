#ifndef __NETWORK_FACTORY_IMPL__
#define __NETWORK_FACTORY_IMPL__

#include <vector>
#include "NetworkImpl.hh"
#include "LayerChainLinkImpl.hh"

using namespace std;
using namespace Eigen;

class NetworkFactoryImpl : public NetworkFactory {
    public:
        explicit NetworkFactoryImpl(LayerFactory& layerFactory)
            : layerFactory(layerFactory) { };

        shared_ptr<Network> makeNetwork(const vector<uint32_t>& sizes) override;

    private:
        shared_ptr<LayerChainLinkImpl> makeLink(
            uint32_t inputSize,
            uint32_t numberOfNeurons);

        shared_ptr<LayerChainLinkImpl> makeLink(
            uint32_t inputSize,
            uint32_t numberOfNeurons,
            shared_ptr<LayerChainLinkImpl> nextLink);

        LayerFactory& layerFactory;
};

#endif