
#include "NetworkImpl.hh"

MatrixXd NetworkImpl::reactToInput(const MatrixXd& lineVector) {
    return (*rootLink).feedForward(lineVector);
}

MatrixXd NetworkImpl::learnFromSample(const MatrixXd& sample) {
    return MatrixXd::Identity(1, sample.cols());
}
