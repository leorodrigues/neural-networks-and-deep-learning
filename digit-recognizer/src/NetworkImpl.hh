#ifndef __NETWORK_IMPL__
#define __NETWORK_IMPL__

#include <utility>

#include "Interfaces.hh"

using namespace std;
using namespace Eigen;

class NetworkImpl : public Network {
    public:
        explicit NetworkImpl(shared_ptr<LayerChainLink> rootLink)
            : rootLink(move(rootLink)) { };

        MatrixXd reactToInput(const MatrixXd& input) override;
        MatrixXd learnFromSample(const MatrixXd& sample) override;

    private:
        shared_ptr<LayerChainLink> rootLink;
};

#endif