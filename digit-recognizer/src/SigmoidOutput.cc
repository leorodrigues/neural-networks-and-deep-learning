#include "SigmoidOutput.hh"

MatrixXd SigmoidOutput::apply(const MatrixXd& lineVector) {
    auto ones = MatrixXd::Ones(1, lineVector.cols());
    return ones.cwiseQuotient(ones + (lineVector * -1).array().exp().matrix());
}

MatrixXd SigmoidOutput::applyDerived(const MatrixXd& lineVector) {
    auto ones = MatrixXd::Ones(1, lineVector.cols());
    auto o = apply(lineVector);
    return o.cwiseProduct(ones - o);
}