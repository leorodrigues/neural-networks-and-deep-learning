#ifndef __SIGMOID_OUTPUT__
#define __SIGMOID_OUTPUT__

#include "Interfaces.hh"

class SigmoidOutput : public OutputFunction {
    public:
        MatrixXd apply(const MatrixXd& netInput) override;
        MatrixXd applyDerived(const MatrixXd& netInput) override;
};

#endif