#include <memory>

#include "gmock/gmock.h"

#include "LayerChainLinkImpl.hh"
#include "../support/Mockery.hh"

using namespace testing;

class LayerChainLinkImplTest : public Test {
    public:
        shared_ptr<LayerMock> layerMock;

        void SetUp() override {
            layerMock = make_shared<LayerMock>();
        }
};

TEST_F(LayerChainLinkImplTest, backPropagateSingleLink) {
    MatrixXd networkInput { 1, 3 };
    networkInput << 0, 0, 1;

    MatrixXd expectation { 1, 3 };
    expectation << 0, 1, 0;

    MatrixXd netInputResult { 1, 3 };
    netInputResult << 0, 1, 1;

    MatrixXd output { 1, 3 };
    output << 1, 0, 0;

    MatrixXd derivedOutput { 1, 3 };
    derivedOutput << 1, 0, 1;

    MatrixXd error { 1, 3 };
    error << 1, 1, 0;

    EXPECT_CALL(*layerMock, computeNetInput(networkInput))
        .Times(1).WillOnce(Return(netInputResult));

    EXPECT_CALL(*layerMock, computeOutput(netInputResult))
        .Times(1).WillOnce(Return(output));

    EXPECT_CALL(*layerMock, computeDerivedOutput(netInputResult))
        .Times(1).WillOnce(Return(derivedOutput));

    EXPECT_CALL(*layerMock, adjustWeightsToBaseError(
        expectation, output, derivedOutput))
            .Times(1).WillOnce(Return(error));

    auto subject = LayerChainLinkImpl(layerMock);

    subject.backPropagate(networkInput, expectation);
}

TEST_F(LayerChainLinkImplTest, backPropagateParentAndChildLinks) {
    MatrixXd networkInput { 1, 4 };
    networkInput << 0, 0, 0, 1;

    MatrixXd expectation { 1, 4 };
    expectation << 0, 0, 1, 0;

    MatrixXd firstLayerNetInput { 1, 4 };
    firstLayerNetInput << 0, 0, 1, 1;

    MatrixXd firstLayerOutput { 1, 4 };
    firstLayerOutput << 0, 1, 0, 0;

    MatrixXd firstLayerDerivedOutput { 1, 4 };
    firstLayerDerivedOutput << 0, 1, 0, 1;

    MatrixXd secondLayerError { 1, 4 };
    secondLayerError << 0, 1, 1, 0;

    MatrixXd secondLayerOutput { 1, 4 };
    secondLayerOutput << 0, 1, 1, 1;

    MatrixXd secondLayerDerivedOutput { 1, 4 };
    secondLayerDerivedOutput << 1, 0, 0, 0;

    MatrixXd secondLayerNetInput { 1, 4 };
    secondLayerNetInput << 1, 0, 0, 1;

    MatrixXd firstLayerError { 1, 4 };
    firstLayerError << 1, 0, 1, 0;


    EXPECT_CALL(*layerMock, computeNetInput(networkInput))
        .Times(1).WillOnce(Return(firstLayerNetInput));

    EXPECT_CALL(*layerMock, computeOutput(firstLayerNetInput))
        .Times(1).WillOnce(Return(firstLayerOutput));

    EXPECT_CALL(*layerMock, computeDerivedOutput(firstLayerNetInput))
        .Times(1).WillOnce(Return(firstLayerDerivedOutput));


    EXPECT_CALL(*layerMock, computeNetInput(firstLayerOutput))
        .Times(1).WillOnce(Return(secondLayerNetInput));

    EXPECT_CALL(*layerMock, computeOutput(secondLayerNetInput))
        .Times(1).WillOnce(Return(secondLayerOutput));

    EXPECT_CALL(*layerMock, computeDerivedOutput(secondLayerNetInput))
        .Times(1).WillOnce(Return(secondLayerDerivedOutput));


    EXPECT_CALL(*layerMock, adjustWeightsToBaseError(
        expectation, secondLayerOutput, secondLayerDerivedOutput))
            .Times(1).WillOnce(Return(secondLayerError));

    EXPECT_CALL(*layerMock, adjustWeightsToIntermediaryError(
        secondLayerError, firstLayerOutput, firstLayerDerivedOutput))
            .Times(1).WillOnce(Return(firstLayerError));

    auto subject = LayerChainLinkImpl(layerMock,
        make_shared<LayerChainLinkImpl>(layerMock));

    subject.backPropagate(networkInput, expectation);
}

TEST_F(LayerChainLinkImplTest, feedForwardSingleLink) {
    MatrixXd networkInput { 1, 5 };
    networkInput << 1, 1, 2, 3, 5;

    MatrixXd netInputResult { 1, 3 };
    netInputResult << 7, 11, 13;

    EXPECT_CALL(*layerMock, computeNetInput(networkInput))
        .Times(1).WillOnce(Return(netInputResult));

    MatrixXd activationResult { 1, 3 };
        activationResult << 8, 13, 21;

    EXPECT_CALL(*layerMock, computeOutput(netInputResult))
        .Times(1).WillOnce(Return(activationResult));

    auto subject = LayerChainLinkImpl(layerMock);

    MatrixXd actualActivation = subject.feedForward(networkInput);

    ASSERT_THAT(actualActivation, Eq(activationResult));
}

TEST_F(LayerChainLinkImplTest, feedForwardParentAndChildLinks) {
    MatrixXd networkInput { 1, 3 };
    networkInput << 1, 2, 3;

    MatrixXd firstNetInputResult { 1, 3 };
    firstNetInputResult << 4, 5, 6;

    EXPECT_CALL(*layerMock, computeNetInput(networkInput))
        .Times(1).WillOnce(Return(firstNetInputResult));

    MatrixXd firstActivationResult { 1, 3 };
    firstActivationResult << 7, 8, 9;

    EXPECT_CALL(*layerMock, computeOutput(firstNetInputResult))
        .Times(1).WillOnce(Return(firstActivationResult));


    MatrixXd secondNetInputResult { 1, 3 };
    secondNetInputResult << 10, 11, 12;

    EXPECT_CALL(*layerMock, computeNetInput(firstActivationResult))
        .Times(1).WillOnce(Return(secondNetInputResult));

    MatrixXd secondActivationResult { 1, 3 };
    secondActivationResult << 13, 14, 15;

    EXPECT_CALL(*layerMock, computeOutput(secondNetInputResult))
        .Times(1).WillOnce(Return(secondActivationResult));

    auto subject = LayerChainLinkImpl(layerMock,
        make_shared<LayerChainLinkImpl>(layerMock));

    MatrixXd actualActivation = subject.feedForward(networkInput);

    ASSERT_THAT(actualActivation, Eq(secondActivationResult));
}
