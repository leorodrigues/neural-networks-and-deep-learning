#include "gmock/gmock.h"

#include "memory"
#include "LayerFactoryImpl.hh"
#include "../support/Mockery.hh"

using namespace testing;

class LayerImplTest: public Test {
    public:
        OutputFunctionMock outputFunctionMock;
        LayerFactoryImpl factory { outputFunctionMock };
};

TEST_F(LayerImplTest, computeOutput) {
    auto fakeActivationResult = MatrixXd(1, 2);
    fakeActivationResult << 11, 13;

    EXPECT_CALL(outputFunctionMock, apply(HasShape(1, 3)))
        .Times(1).WillOnce(Return(fakeActivationResult));

    auto layer = factory.makeLayer(3, 2);
    auto layerOutput = (*layer).computeOutput(MatrixXd::Random(1, 3));

    ASSERT_THAT(layerOutput, Eq(fakeActivationResult));
}

TEST_F(LayerImplTest, computeNetInput) {
    auto layer = factory.makeLayer(3, 2);
    auto layerOutput = (*layer).computeNetInput(MatrixXd::Random(1, 3));

    ASSERT_THAT(layerOutput, HasShape(1, 2));
}
