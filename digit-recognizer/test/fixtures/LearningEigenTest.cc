#include <Interfaces.hh>
#include "gmock/gmock.h"

#include "memory"
#include "eigen3/Eigen/Eigen"

using namespace std;
using namespace testing;
using namespace Eigen;

class LearningEigenTest: public Test { };

TEST_F(LearningEigenTest, elementWiseQuotients) {
    auto twos = MatrixXd(1, 3);
    twos << 2, 2, 2;

    auto expected = MatrixXd(1, 3);
    expected << .5, .5, .5;

    auto ones = MatrixXd::Ones(1, 3);

    ASSERT_THAT(ones.cwiseQuotient(twos), Eq(expected));
}

TEST_F(LearningEigenTest, elementWiseProductOfLineVector) {
    auto leftMatrix = MatrixXd(1, 3);
    leftMatrix << 1, 2, 3;

    auto rightMatrix = MatrixXd(1, 3);
    rightMatrix << 5, 7, 11;

    auto expectedProduct = MatrixXd(1, 3);
    expectedProduct << 5, 14, 33;

    auto product = leftMatrix.cwiseProduct(rightMatrix);

    ASSERT_THAT(product, Eq(expectedProduct));
}

TEST_F(LearningEigenTest, elementWiseProductOfColumnVector) {
    auto leftMatrix = MatrixXd(3, 1);
    leftMatrix << 1, 2, 3;

    auto rightMatrix = MatrixXd(3, 1);
    rightMatrix << 5, 7, 11;

    auto expectedProduct = MatrixXd(3, 1);
    expectedProduct << 5, 14, 33;

    auto product = leftMatrix.cwiseProduct(rightMatrix);

    ASSERT_THAT(product, Eq(expectedProduct));
}
