#include <memory>

#include "gmock/gmock.h"

#include "NetworkImpl.hh"
#include "../support/Mockery.hh"

using namespace testing;
using namespace std;

class NetworkImplTest: public Test {
    public:
        shared_ptr<LayerChainLinkMock> rootLink;
        unique_ptr<NetworkImpl> subject;

        void SetUp() override {
            rootLink = make_shared<LayerChainLinkMock>();
            subject = unique_ptr<NetworkImpl>(new NetworkImpl(rootLink));
        }
};

TEST_F(NetworkImplTest, reactToInput) {
    MatrixXd expectedLayerInput { 1, 6 };
    expectedLayerInput << 1, 1, 2, 3, 5, 8;

    MatrixXd layerOutput { 1, 6 };
    layerOutput << 13, 21, 34, 55, 89, 144;

    EXPECT_CALL(*rootLink, feedForward(expectedLayerInput))
        .Times(1).WillOnce(Return(layerOutput));

    MatrixXd networkInput { 1, 6 };
    networkInput << 1, 1, 2, 3, 5, 8;

    auto networkOutput = subject->reactToInput(networkInput);

    EXPECT_THAT(networkOutput, Eq(layerOutput));
}
