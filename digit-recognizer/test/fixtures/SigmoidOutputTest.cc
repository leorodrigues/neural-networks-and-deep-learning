#include "gmock/gmock.h"

#include "SigmoidOutput.hh"
#include <iostream>

using namespace std;
using namespace testing;

class SigmoidOutputTest : public Test {
    public:
        SigmoidOutput subject { };
};

TEST_F(SigmoidOutputTest, applyShouldComputeTheLogisticFn) {
    auto expectedOutput = MatrixXd(1, 3);
    expectedOutput.setConstant(1.0 / (1.0 + exp(-1.0)));
    ASSERT_THAT(subject.apply(MatrixXd::Ones(1, 3)), Eq(expectedOutput));
}

TEST_F(SigmoidOutputTest, applyDerivedShouldComputeTheDerivedLogisticFn) {
    double_t output = 1.0 / (1.0 + exp(-1.0));
    output = output * (1 - output);

    auto expectedOutput = MatrixXd(1, 3);
    expectedOutput.setConstant(output);

    ASSERT_THAT(subject.applyDerived(MatrixXd::Ones(1, 3)), Eq(expectedOutput));
}