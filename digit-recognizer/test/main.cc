#include "gmock/gmock.h"

using namespace testing;

int main(int argCounter, char** argVector) {
    GTEST_FLAG(throw_on_failure) = true;
    GTEST_FLAG(catch_exceptions) = false;
    GTEST_FLAG(print_time) = true;
    GTEST_FLAG(output) = "xml";

    InitGoogleMock(&argCounter, argVector);

    return RUN_ALL_TESTS();
}
