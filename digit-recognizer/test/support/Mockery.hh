#ifndef __TEST_SUPPORT__
#define __TEST_SUPPORT__

#include "gmock/gmock.h"
#include "Interfaces.hh"

using namespace std;
using namespace testing;

class OutputFunctionMock : public OutputFunction {
    public:
        MOCK_METHOD1(apply, MatrixXd(const MatrixXd& lineVector));
        MOCK_METHOD1(applyDerived, MatrixXd(const MatrixXd& lineVector));
};

class LayerFactoryMock : public LayerFactory {
    public:
        MOCK_METHOD2(makeLayer, shared_ptr<Layer>(
            uint32_t inputSize,
            uint32_t numberOfNeurons));
};

class LayerMock : public Layer {
    public:
        MOCK_METHOD1(computeNetInput, MatrixXd(
            const MatrixXd& input));

        MOCK_METHOD1(computeOutput, MatrixXd(
            const MatrixXd& netInput));

        MOCK_METHOD1(computeDerivedOutput, MatrixXd(
            const MatrixXd& netInput));

        MOCK_METHOD1(adjustWeights, MatrixXd(
            const MatrixXd& errors));

        MOCK_METHOD3(adjustWeightsToBaseError, MatrixXd(
            const MatrixXd& expectation,
            const MatrixXd& output,
            const MatrixXd& derivedOutput));

        MOCK_METHOD3(adjustWeightsToIntermediaryError, MatrixXd(
            const MatrixXd& nextLayerError,
            const MatrixXd& output,
            const MatrixXd& derivedOutput));
};

class LayerChainLinkMock : public LayerChainLink {
    public:
        MOCK_METHOD1(feedForward, MatrixXd(
            const MatrixXd& input));

        MOCK_METHOD2(backPropagate, void(
            const MatrixXd& input,
            const MatrixXd& expectations));
};

#define OUT_OF_SHAPE_ERROR_MSG string("Input is out of shape") \
    + " [" + PrintToString(rows) + "; " + PrintToString(cols) + "]"

MATCHER_P2(HasShape, rows, cols, OUT_OF_SHAPE_ERROR_MSG) {
    return arg.rows() == rows && arg.cols() == cols;
}

#endif
