nodemon \
    -w ./digit-recognizer \
    -w CMakeLists.txt \
    -e txt,cpp,hpp,cc,hh \
    --exec ./test.sh