#!/bin/bash

[ "$1" == 'clean-first' ] && rm -rf ./build

(cmake -B./build . \
    && make -j3 -C ./build \
    && chmod +x `find ./ -name *-test` \
    && echo \
    && for f in $(find ./ -name *-test); do $f; done) || exit 1
